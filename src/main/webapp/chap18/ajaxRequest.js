/*
 * Check browser type and create ajaxRequest object
	Put this function in an external.js file and use it for your Ajax programs
 */
function CreateRequestObject() { 																//1
	var ajaxRequest;																							//2
	//The variable that makes Ajax possible!
	try {																													//3
		//Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest(); 												//4.Create the object
	} catch (e) {
			//Internet Explorer Browsers 
		try {
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");				//5
		} catch (e) {
				try {
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
						return false; 
				}
		}
	}
	return ajaxRequest;																						//6
} //End function
/*
 * Explanation
  1 The CreateRequest Object function creates the XMLHttpRequest object for Ajax to 
		set up communication between JavaScript and the server.
	2 This variable will be used to hold a reference to a new Ajax XMLHttpRequest object 
		created in this function.
	3 If the try block has no errors, its statements will be executed; otherwise, the catch 
		block will be executed.
	4 For  most  modern  browsers,  this try will  succeed.  Here  the XMLHttpRequest() 
		method will create a new object called ajaxRequest. (The object name is named 
		any valid variable name.)
	5 The catch blocks are executed if the browser is Microsoft Internet Explorer.
		Internet Explorer uses the ActiveXObject() method to create the Ajax object rather than 
		the XMLHttpRequest() method.
6 If successful, an Ajax request object will be returned from this function.
 */